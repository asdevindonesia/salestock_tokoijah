Prerequisite Library/Framework
1. go get github.com/gorilla/mux
2. go get github.com/mattn/go-sqlite3

File/Directory Structure
README.md : this file
csv : contains files for data migration
ddl.sql : file DDL untuk database
gui : contains CMS UI source code
golang : contains files for backend server

golang/src contains
main.go is the main file
migration.gtpl is the html page for migration
tokoijah.db is sqlite database
web is directory contains compiled CMS UI
run.sh is script to run server
main is executable main.go compiled using OSX

How to Run using script
Please go to golang/src from terminal
execute "./run.sh"

How to Compile
Please go to golang/src from terminal
execute "go build -o main"

How to Run
Please go to golang/src from terminal
execute "./main"

It is compiled in OSX

Asumsi
1. ID Pesanan pada Laporan Penjualan diganti dengan ID pada Catatan Barang Keluar karena memisahkan kolom Catatan memakan waktu yang cukup lama, karena tiap ID Pesanan berbeda maka di Laporan Penjualan tidak ada Pembelian lebih dari 2 barang yang berbeda
2. Jumlah Sekarang di Catatan Jumlah Barang adalah  Catatang Barang Masuk (Jumlah Diterima) - Catatan Barang Keluar (Jumlah Keluar) 
3. Jumlah pada Laporan Nilai Barang menggunakan Jumlah Sekarang di Catatan Jumlah Barang
4. Nilai total dan Rata-Rata pada Laporan Nilai Barang dibulatkan menjadi integer
5. Data kembalian yang berupa tanggal belum terformat
6. Data kembalian yang berupa harga/jumlah belum terformat
7. Waktu di Catatan Barang Masuk/Keluar merupakan tanggal dari sistem

CMS UI
http://localhost:8080


Catatan Jumlah Barang
Api
GET /stocks - Dapatkan semua jumlah barang
	return value null atau array stock
	curl -i -X GET http://localhost:8080/stocks

POST /stocks - Tambah jumlah barang
	parameters sku, description
	return stock
	Example : curl -i -X POST -d "sku=1&description=Baju" http://localhost:8080/stocks

PUT /stocks/{sku} - Ubah jumlah barang berdasarkan sku
	parameters description
	return stock
	Example : curl -i -X PUT -d "description=Tip 3" http://localhost:8080/stocks/1

GET /stocks/{sku} - Dapatkan jumlah barang berdasarkan sku
	return stock
	Example : curl -i -X GET http://localhost:8080/stocks/1

DELETE /stocks/{sku} - Hapus jumlah barang berdasarkan sku
	return nil
	Example : curl -i -X DELETE http://localhost:8080/stocks/1
	
	Untuk Stock saya tidak suka dengan delete karena bisa menghilangkan record di database,
	usul lebih lanjut bisa menggunakan 1 flag menandakan stock nya bisa digunakan atau tidak

Catatan Barang Masuk
Api
GET /incoming - Dapatkan semua catatan barang masuk
	return value null atau array incoming
	curl -i -X GET http://localhost:8080/incoming

POST /incoming - Tambah catatan barang masuk
	parameters sku, ordered, received, price, receipt, note
	return incoming
	Example : curl -i -X POST -d "sku=1&ordered=2&received=3&price=20500&receipt=2017&note=test" http://localhost:8080/incoming

PUT /incoming/{id} - Ubah jumlah barang berdasarkan sku
	parameters sku, ordered, received, price, receipt, note
	return incoming
	Example : curl -i -X PUT -d "sku=1&ordered=3&received=3&price=20500&receipt=2017&note=test" http://localhost:8080/incoming/1

GET /incoming/{id} - Dapatkan jumlah barang berdasarkan sku
	return incoming
	Example : curl -i -X GET http://localhost:8080/incoming/1

DELETE /incoming/{id} - Hapus jumlah barang berdasarkan sku
	return nil
	Example : curl -i -X DELETE http://localhost:8080/incoming/1

Catatan Barang Keluar
Api
GET /outgoing - Dapatkan semua catatan barang masuk
	return value null atau array outgoing
	curl -i -X GET http://localhost:8080/outgoing

POST /outgoing - Tambah catatan barang masuk
	parameters sku, out, price, note
	return outgoing
	Example : curl -i -X POST -d "sku=1&out=2&price=22500&note=great" http://localhost:8080/outgoing

PUT /outgoing/{id} - Ubah jumlah barang berdasarkan sku
	parameters description, amount
	return outgoing
	Example : curl -i -X PUT -d "sku=1&out=2&price=25500&note=cheers" http://localhost:8080/outgoing/1

GET /outgoing/{id} - Dapatkan jumlah barang berdasarkan sku
	return outgoing
	Example : curl -i -X GET http://localhost:8080/outgoing/1

DELETE /outgoing/{id} - Hapus jumlah barang berdasarkan sku
	return nil
	Example : curl -i -X DELETE http://localhost:8080/outgoing/1

Laporan Nilai Barang
	curl -i -X GET http://localhost:8080/report/inventory

Laporan Penjualan
	curl -i -X GET http://localhost:8080/report/selling

Laporan Penjualan per bulan
	curl -i -X GET http://localhost:8080/report/selling/<bulan>-<tahun>

Migration
	http://localhost:8080/migration
	Example data in csv directory
	1. stocks.csv for Catatan Jumlah Barang
	2. incoming.csv for Catatan Barang Masuk
	3. outgoing.csv for Catatan Barang Keluar

