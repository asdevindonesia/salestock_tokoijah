/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
 define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'promise', 'ojs/ojtable', 'ojs/ojarraydataprovider', 'ojs/ojinputtext', 'ojs/ojinputnumber', 'ojs/ojdatacollection-utils', 'ojs/ojbutton', 'ojs/ojdialog', 'ojs/ojselectcombobox'],
   function(oj, ko, $) {
    
    function IncomingViewModel() {
      var self = this;
      // self.server = "http://localhost:8080/incoming";
      // self.stocksServer = "http://localhost:8080/stocks";
      self.server = "incoming";
      self.stocksServer = "stocks";
      self.selectStock = ko.observable();
      self.ordered = ko.observable(0);
      self.received = ko.observable(0);
      self.price = ko.observable(0);
      self.receipt = ko.observable("");
      self.note = ko.observable("");

      self.stocks = ko.observableArray([]);
      self.stocksDP = new oj.ArrayDataProvider(self.stocks, {idAttribute: 'sku'});
      self.observableArray = ko.observableArray([]);
      self.dataprovider = new oj.ArrayDataProvider(self.observableArray, {idAttribute: 'id'});

      self.loadIncoming = function() {
        $.getJSON(self.server,
          function (data) {
            if (data != null)
            {
              self.observableArray.removeAll();
              ko.utils.arrayPushAll(self.observableArray, data);
            }
          });   
      };
      
      // Below are a subset of the ViewModel methods invoked by the ojModule binding
      // Please reference the ojModule jsDoc for additional available methods.

      /**
       * Optional ViewModel method invoked when this ViewModel is about to be
       * used for the View transition.  The application can put data fetch logic
       * here that can return a Promise which will delay the handleAttached function
       * call below until the Promise is resolved.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @return {Promise|undefined} - If the callback returns a Promise, the next phase (attaching DOM) will be delayed until
       * the promise is resolved
       */
       self.handleActivated = function(info) {
        self.loadIncoming();
      };

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {boolean} info.fromCache - A boolean indicating whether the module was retrieved from cache.
       */
       self.handleAttached = function(info) {
        self.addDialog = document.querySelector("#addIncomingDialog");
        self.incomingTable = document.querySelector("#incomingTable");
      };


      /**
       * Optional ViewModel method invoked after the bindings are applied on this View. 
       * If the current View is retrieved from cache, the bindings will not be re-applied
       * and this callback will not be invoked.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       */
       self.handleBindingsApplied = function(info) {
        // Implement if needed
      };

      /*
       * Optional ViewModel method invoked after the View is removed from the
       * document DOM.
       * @param {Object} info - An object with the following key-value pairs:
       * @param {Node} info.element - DOM element or where the binding is attached. This may be a 'virtual' element (comment node).
       * @param {Function} info.valueAccessor - The binding's value accessor.
       * @param {Array} info.cachedNodes - An Array containing cached nodes for the View if the cache is enabled.
       */
       self.handleDetached = function(info) {
        // Implement if needed
      };

      self.addClick = function(event) {

        $.getJSON(self.stocksServer,
          function (data) {
            if (data != null)
            {
              self.stocks.removeAll();
              for (i = 0; i < data.length; i++) 
              { 
                var stock = data[i];
                self.stocks.push({value: stock.sku,  label: stock.description + "(" + stock.sku + ")"});
              }
              self.ordered = ko.observable(0);
              self.received = ko.observable(0);
              self.price = ko.observable(0);
              self.receipt = ko.observable("");
              self.note = ko.observable("");
              self.addDialog.open();
            }
          });   
        return true;
      }

      self.refreshClick = function(event) {
        self.loadIncoming();
        return true;
      }

      self.deleteClick = function(event) {
        var values = self.incomingTable.selection;
        if (values.length > 0)
        {
          var key = values[0].startKey.row;
          $.ajax({
            type: "DELETE",
            url: self.server + "/" + key,
            success: function (data) {
              self.loadIncoming();
            }
          });
        }
        return true;
      }

      self.saveClick = function(event) {
       $.ajax({
        type: "POST",
        url: self.server,
        data: {"sku" : self.selectStock(), "ordered" : self.ordered(), "received" : self.received(), "price" : self.price, "receipt" : self.receipt(), "note" : self.note()},
        success: function (data) {
          self.loadIncoming();
          self.addDialog.close();
        }
      });
       return true;
     }

     self.cancelClick = function(event) {
      self.addDialog.close();
      return true;
    }

    self._editRowRenderer = oj.KnockoutTemplateUtils.getRenderer('editRowTemplate', true);
    self._navRowRenderer = oj.KnockoutTemplateUtils.getRenderer('rowTemplate', true);
    self.rowRenderer = function(context)
    {
      var mode = context['rowContext']['mode'];
      var renderer;
      
      if (mode === 'edit')
      {
        self._editRowRenderer(context);
      }
      else if (mode === 'navigation')
      {
        self._navRowRenderer(context);
      }
    };
    
    this.beforeRowEditEndListener = function(event)
    {
     var data = event.detail;
     var rowIdx = data.rowContext.status.rowIndex;
     self.dataprovider.fetchByOffset({offset: rowIdx}).then(function(value)
     {
       var row = value['results'][0]['data'];
       $.ajax({
        type: "PUT",
        url: self.server + "/" + row.id,
        data: row,
        success: function (data) {
          self.loadIncoming();
        }
      });
     });
     if (oj.DataCollectionEditUtils.basicHandleRowEditEnd(event, data) === false) {
       event.preventDefault();
     }
   }

 }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
     return new IncomingViewModel();
   }
   );
