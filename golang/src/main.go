package main

import (
	"database/sql"
	"encoding/csv"
	"encoding/json"
	"flag"
	_ "fmt"
	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
	"html/template"
	_ "io"
	"log"
	"net/http"
	_ "os"
	"strconv"
	"strings"
	"time"
)

type Stock struct {
	SKU         string `json:"sku"`
	Description string `json:"description"`
	Amount      int    `json:"amount"`
}

type Inventory struct {
	SKU         string `json:"sku"`
	Description string `json:"description"`
	Amount      int    `json:"amount"`
	Total       int    `json:"total"`
	Average     int    `json:"average"`
}

type Incoming struct {
	Id          int       `json:"id"`
	SKU         string    `json:"sku"`
	Description string    `json:"description"`
	Ordered     int       `json:"ordered"`
	Received    int       `json:"received"`
	Receipt     string    `json:"receipt"`
	Price       float64   `json:"price"`
	Note        string    `json:"note"`
	Time        time.Time `json:"time"`
	Status      int       `json:"status"`
	Total       float64   `json:"total"`
}

type Outgoing struct {
	Id          int       `json:"id"`
	SKU         string    `json:"sku"`
	Description string    `json:"description"`
	Out         int       `json:"out"`
	Price       float64   `json:"price"`
	Note        string    `json:"note"`
	Time        time.Time `json:"time"`
	Total       float64   `json:"total"`
}

func main() {
	var dir string

	flag.StringVar(&dir, "dir", "web", "the directory to serve files from. Defaults to the current dir")
	flag.Parse()

	router := mux.NewRouter()
	router.HandleFunc("/stocks", GetStocks).Methods("GET")
	router.HandleFunc("/stocks", CreateStock).Methods("POST")
	router.HandleFunc("/stocks/upload", UploadStocks).Methods("POST")
	router.HandleFunc("/stocks/{sku}", GetStock).Methods("GET")
	router.HandleFunc("/stocks/{sku}", UpdateStock).Methods("PUT")
	router.HandleFunc("/stocks/{sku}", DeleteStock).Methods("DELETE")

	router.HandleFunc("/incoming", GetIncomings).Methods("GET")
	router.HandleFunc("/incoming", CreateIncoming).Methods("POST")
	router.HandleFunc("/incoming/upload", UploadIncoming).Methods("GET", "POST")
	router.HandleFunc("/incoming/{id}", GetIncoming).Methods("GET")
	router.HandleFunc("/incoming/{id}", UpdateIncoming).Methods("PUT")
	router.HandleFunc("/incoming/{id}", DeleteIncoming).Methods("DELETE")

	router.HandleFunc("/outgoing", GetOutgoings).Methods("GET")
	router.HandleFunc("/outgoing", CreateOutgoing).Methods("POST")
	router.HandleFunc("/outgoing/upload", UploadOutgoing).Methods("GET", "POST")
	router.HandleFunc("/outgoing/{id}", GetOutgoing).Methods("GET")
	router.HandleFunc("/outgoing/{id}", UpdateOutgoing).Methods("PUT")
	router.HandleFunc("/outgoing/{id}", DeleteOutgoing).Methods("DELETE")

	router.HandleFunc("/report/inventory", GetInventory).Methods("GET")
	router.HandleFunc("/report/selling", GetSelling).Methods("GET")
	router.HandleFunc("/report/selling/{id}", GetSellingMonthly).Methods("GET")

	router.HandleFunc("/migration", Migration).Methods("GET")

	router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir(dir))))
	log.Fatal(http.ListenAndServe(":8080", router))
}

func checkError(w http.ResponseWriter, err error) {
	if err != nil {
		log.Println(err)
		http.Error(w, "Bad Request", 400)
	}
}

func OpenDB() (*sql.DB, error) {
	return sql.Open("sqlite3", "./tokoijah.db")
}

func GetStocks(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDB()
	checkError(w, err)
	rows, err := db.Query("SELECT SKU, DESCRIPTION, AMOUNT FROM STOCK_VALUE_VIEW")
	checkError(w, err)

	var stocks []Stock
	var sku string
	var description string
	var amount int

	for rows.Next() {
		err = rows.Scan(&sku, &description, &amount)
		checkError(w, err)
		stocks = append(stocks, Stock{SKU: sku, Description: description, Amount: amount})
	}

	rows.Close()
	db.Close()
	json.NewEncoder(w).Encode(stocks)
}

func CreateStock(w http.ResponseWriter, r *http.Request) {
	sku := r.FormValue("sku")
	description := r.FormValue("description")

	if len(sku) == 0 {
		http.Error(w, "Bad Request", 400)
		return
	}

	db, err := OpenDB()
	checkError(w, err)

	stmt, err := db.Prepare("INSERT INTO STOCK(SKU, DESCRIPTION) VALUES ($1,$2)")
	checkError(w, err)

	_, err = stmt.Exec(sku, description)
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	db.Close()
	GetStockById(sku, w, r)
}

func GetStock(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["sku"]
	GetStockById(id, w, r)
}

func GetStockById(id string, w http.ResponseWriter, r *http.Request) {

	db, err := OpenDB()
	checkError(w, err)

	row := db.QueryRow("SELECT SKU, DESCRIPTION, AMOUNT FROM STOCK_VALUE_VIEW WHERE SKU=$1", id)
	var sku string
	var description string
	var amount int

	var stock Stock

	switch err = row.Scan(&sku, &description, &amount); err {
	case nil:
		stock = Stock{SKU: sku, Description: description, Amount: amount}
		db.Close()
		json.NewEncoder(w).Encode(stock)
	default:
		db.Close()
		http.NotFound(w, r)
	}

}

func UpdateStock(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	sku := params["sku"]
	description := r.FormValue("description")

	db, err := OpenDB()
	checkError(w, err)

	stmt, err := db.Prepare("UPDATE STOCK SET DESCRIPTION=$1 WHERE SKU=$2")
	checkError(w, err)

	_, err = stmt.Exec(description, sku)
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	db.Close()
	GetStock(w, r)
}

func DeleteStock(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	sku := params["sku"]
	db, err := OpenDB()
	checkError(w, err)

	stmt, err := db.Prepare("DELETE FROM STOCK WHERE SKU=$1")
	checkError(w, err)

	_, err = stmt.Exec(sku)
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	db.Close()
	json.NewEncoder(w).Encode(nil)
}

func GetIncomings(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDB()
	checkError(w, err)
	rows, err := db.Query("SELECT ID, SKU, DESCRIPTION, ORDERED, RECEIVED, PRICE, RECEIPT, NOTE, DATE_UPDATED, STATUS, TOTAL FROM INCOMING_VIEW")
	checkError(w, err)

	var incomings []Incoming
	var id int
	var sku string
	var description string
	var ordered int
	var received int
	var price float64
	var receipt string
	var note string
	var time time.Time
	var status int
	var total float64

	for rows.Next() {
		err = rows.Scan(&id, &sku, &description, &ordered, &received, &price, &receipt, &note, &time, &status, &total)
		checkError(w, err)
		incomings = append(incomings, Incoming{Id: id, SKU: sku, Description: description, Ordered: ordered, Received: received, Price: price, Receipt: receipt, Note: note, Time: time, Status: status, Total: total})
	}

	rows.Close()
	db.Close()
	json.NewEncoder(w).Encode(incomings)
}

func CreateIncoming(w http.ResponseWriter, r *http.Request) {
	sku := r.FormValue("sku")
	ordered := r.FormValue("ordered")
	received := r.FormValue("received")
	price := r.FormValue("price")
	receipt := r.FormValue("receipt")
	note := r.FormValue("note")

	o, err := strconv.Atoi(ordered)
	checkError(w, err)
	if err != nil {
		return
	}

	re, err := strconv.Atoi(received)
	checkError(w, err)
	if err != nil {
		return
	}

	p, err := strconv.ParseFloat(price, 64)
	checkError(w, err)
	if err != nil {
		return
	}

	db, err := OpenDB()
	checkError(w, err)

	stmt, err := db.Prepare("INSERT INTO INCOMING(SKU, ORDERED, RECEIVED, PRICE, RECEIPT, NOTE) VALUES ($1,$2,$3,$4,$5,$6)")
	checkError(w, err)

	res, err := stmt.Exec(sku, o, re, p, receipt, note)
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	id, err := res.LastInsertId()
	if err != nil {
		db.Close()
		return
	}

	db.Close()
	GetIncomingById(int(id), w, r)
}

func GetIncoming(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	i, err := strconv.Atoi(id)
	checkError(w, err)
	if err != nil {
		return
	}

	GetIncomingById(i, w, r)
}

func GetIncomingById(i int, w http.ResponseWriter, r *http.Request) {

	db, err := OpenDB()
	checkError(w, err)

	row := db.QueryRow("SELECT ID, SKU, DESCRIPTION, ORDERED, RECEIVED, PRICE, RECEIPT, NOTE, DATE_UPDATED, STATUS, TOTAL FROM INCOMING_VIEW WHERE ID=$1", i)
	var id int
	var sku string
	var description string
	var ordered int
	var received int
	var price float64
	var receipt string
	var note string
	var time time.Time
	var status int
	var total float64

	var incoming Incoming

	switch err = row.Scan(&id, &sku, &description, &ordered, &received, &price, &receipt, &note, &time, &status, &total); err {
	case nil:
		incoming = Incoming{Id: id, SKU: sku, Description: description, Ordered: ordered, Received: received, Price: price, Receipt: receipt, Note: note, Time: time, Status: status, Total: total}
		db.Close()
		json.NewEncoder(w).Encode(incoming)
	default:
		db.Close()
		http.NotFound(w, r)
	}

}

func UpdateIncoming(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	id := params["id"]
	i, err := strconv.Atoi(id)
	checkError(w, err)
	if err != nil {
		return
	}

	sku := r.FormValue("sku")
	ordered := r.FormValue("ordered")
	received := r.FormValue("received")
	price := r.FormValue("price")
	receipt := r.FormValue("receipt")
	note := r.FormValue("note")

	o, err := strconv.Atoi(ordered)
	checkError(w, err)
	if err != nil {
		return
	}

	re, err := strconv.Atoi(received)
	checkError(w, err)
	if err != nil {
		return
	}

	p, err := strconv.ParseFloat(price, 64)
	checkError(w, err)
	if err != nil {
		return
	}

	db, err := OpenDB()
	checkError(w, err)

	stmt, err := db.Prepare("UPDATE INCOMING SET SKU=$1, ORDERED=$2, RECEIVED=$3, PRICE=$4, RECEIPT=$5, NOTE=$6 WHERE ID=$7")
	checkError(w, err)

	_, err = stmt.Exec(sku, o, re, p, receipt, note, i)
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	db.Close()
	GetIncoming(w, r)
}

func DeleteIncoming(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	i, err := strconv.Atoi(id)
	checkError(w, err)
	if err != nil {
		return
	}

	db, err := OpenDB()
	checkError(w, err)

	stmt, err := db.Prepare("DELETE FROM Incoming WHERE ID=$1")
	checkError(w, err)

	_, err = stmt.Exec(i)
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	db.Close()
	json.NewEncoder(w).Encode(nil)
}

func GetOutgoings(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDB()
	checkError(w, err)
	rows, err := db.Query("SELECT ID, SKU, DESCRIPTION, OUT, PRICE, NOTE, DATE_UPDATED, TOTAL FROM OUTGOING_VIEW")
	checkError(w, err)

	var outgoings []Outgoing
	var id int
	var sku string
	var description string
	var out int
	var price float64
	var note string
	var time time.Time
	var total float64

	for rows.Next() {
		err = rows.Scan(&id, &sku, &description, &out, &price, &note, &time, &total)
		checkError(w, err)
		outgoings = append(outgoings, Outgoing{Id: id, SKU: sku, Description: description, Out: out, Price: price, Note: note, Time: time, Total: total})
	}

	rows.Close()
	db.Close()
	json.NewEncoder(w).Encode(outgoings)
}

func CreateOutgoing(w http.ResponseWriter, r *http.Request) {
	sku := r.FormValue("sku")
	out := r.FormValue("out")
	price := r.FormValue("price")
	note := r.FormValue("note")

	o, err := strconv.Atoi(out)
	checkError(w, err)
	if err != nil {
		return
	}

	p, err := strconv.ParseFloat(price, 64)
	checkError(w, err)
	if err != nil {
		return
	}

	db, err := OpenDB()
	checkError(w, err)

	stmt, err := db.Prepare("INSERT INTO OUTGOING(SKU, OUT, PRICE, NOTE) VALUES ($1,$2,$3,$4)")
	checkError(w, err)

	res, err := stmt.Exec(sku, o, p, note)
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	id, err := res.LastInsertId()
	if err != nil {
		db.Close()
		return
	}

	db.Close()
	GetOutgoingById(int(id), w, r)
}

func GetOutgoing(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	i, err := strconv.Atoi(id)
	checkError(w, err)
	if err != nil {
		return
	}

	GetOutgoingById(i, w, r)
}

func GetOutgoingById(i int, w http.ResponseWriter, r *http.Request) {

	db, err := OpenDB()
	checkError(w, err)

	row := db.QueryRow("SELECT ID, SKU, DESCRIPTION, OUT, PRICE, NOTE, DATE_UPDATED, TOTAL FROM OUTGOING_VIEW WHERE ID=$1", i)
	var id int
	var sku string
	var description string
	var out int
	var price float64
	var note string
	var time time.Time
	var total float64

	var outgoing Outgoing

	switch err = row.Scan(&id, &sku, &description, &out, &price, &note, &time, &total); err {
	case nil:
		outgoing = Outgoing{Id: id, SKU: sku, Description: description, Out: out, Price: price, Note: note, Time: time, Total: total}
		db.Close()
		json.NewEncoder(w).Encode(outgoing)
	default:
		db.Close()
		http.NotFound(w, r)
	}

}

func UpdateOutgoing(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	id := params["id"]
	i, err := strconv.Atoi(id)
	checkError(w, err)
	if err != nil {
		return
	}

	sku := r.FormValue("sku")
	out := r.FormValue("out")
	price := r.FormValue("price")
	note := r.FormValue("note")

	o, err := strconv.Atoi(out)
	checkError(w, err)
	if err != nil {
		return
	}

	p, err := strconv.ParseFloat(price, 64)
	checkError(w, err)
	if err != nil {
		return
	}

	db, err := OpenDB()
	checkError(w, err)

	stmt, err := db.Prepare("UPDATE OUTGOING SET SKU=$1, OUT=$2, PRICE=$3, NOTE=$4 WHERE ID=$5")
	checkError(w, err)

	_, err = stmt.Exec(sku, o, p, note, i)
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	db.Close()
	GetOutgoing(w, r)
}

func DeleteOutgoing(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]

	i, err := strconv.Atoi(id)
	checkError(w, err)
	if err != nil {
		return
	}

	db, err := OpenDB()
	checkError(w, err)

	stmt, err := db.Prepare("DELETE FROM Outgoing WHERE ID=$1")
	checkError(w, err)

	_, err = stmt.Exec(i)
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	db.Close()
	json.NewEncoder(w).Encode(nil)
}

func GetInventory(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDB()
	checkError(w, err)
	rows, err := db.Query("SELECT SKU, DESCRIPTION, TOTAL, AMOUNT, AVERAGE FROM INVENTORY_VIEW")
	checkError(w, err)

	var inventories [][]string
	var sku string
	var description string
	var total int
	var amount int
	var average int

	skuCount := 0
	amountTotal := 0
	totalTotal := 0

	for rows.Next() {
		err = rows.Scan(&sku, &description, &total, &amount, &average)
		checkError(w, err)
		skuCount++
		amountTotal += amount
		totalTotal += total

		inventories = append(inventories, []string{sku, description, strconv.Itoa(amount), strconv.Itoa(average), strconv.Itoa(total)})
	}

	w.Header().Set("Content-Disposition", "attachment; filename=laporan nilai barang.csv")
	w.Header().Set("Content-Type", "text/csv")
	writer := csv.NewWriter(w)
	writer.Write([]string{"LAPORAN NILAI BARANG"})
	writer.Write([]string{""})
	writer.Write([]string{"Tanggal Cetak", time.Now().String()})
	writer.Write([]string{"Jumlah SKU", strconv.Itoa(skuCount)})
	writer.Write([]string{"Jumlah Total Barang", strconv.Itoa(amountTotal)})
	writer.Write([]string{"Total Nilai", strconv.Itoa(totalTotal)})
	writer.Write([]string{""})
	writer.Write([]string{"SKU", "Nama Item", "Jumlah", "Rata-Rata Harga Beli", "Total"})
	writer.WriteAll(inventories)

	rows.Close()
	db.Close()
	writer.Flush()
}

func GetSelling(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDB()
	checkError(w, err)
	rows, err := db.Query("SELECT ID, DATE_UPDATED, SKU, DESCRIPTION, OUT, PRICE, TOTAL, AVERAGE, REVENUE FROM OUTGOING_REPORT_VIEW")
	checkError(w, err)

	var selling [][]string
	var id int
	var date time.Time
	var sku string
	var description string
	var out int
	var price int
	var total int
	var average int
	var revenue int

	idCount := 0
	revenueTotal := 0
	totalTotal := 0
	outTotal := 0

	for rows.Next() {
		err = rows.Scan(&id, &date, &sku, &description, &out, &price, &total, &average, &revenue)
		checkError(w, err)
		idCount++
		revenueTotal += revenue
		totalTotal += total
		outTotal += out

		selling = append(selling, []string{strconv.Itoa(id), date.String(), sku, description, strconv.Itoa(out), strconv.Itoa(price), strconv.Itoa(total), strconv.Itoa(average), strconv.Itoa(revenue)})
	}

	w.Header().Set("Content-Disposition", "attachment; filename=laporan penjualan.csv")
	w.Header().Set("Content-Type", "text/csv")
	writer := csv.NewWriter(w)
	writer.Write([]string{"LAPORAN PENJUALAN"})
	writer.Write([]string{""})
	writer.Write([]string{"Tanggal Cetak", time.Now().String()})
	writer.Write([]string{"Total Omzet", strconv.Itoa(totalTotal)})
	writer.Write([]string{"Total Laba Kotor", strconv.Itoa(revenueTotal)})
	writer.Write([]string{"Total Penjualan", strconv.Itoa(idCount)})
	writer.Write([]string{"Total Barang", strconv.Itoa(outTotal)})
	writer.Write([]string{""})
	writer.Write([]string{"ID Pesanan", "Waktu", "SKU", "Nama Barang", "Jumlah", "Harga Jual", "Total", "Harga Beli", "Laba"})
	writer.WriteAll(selling)

	rows.Close()
	db.Close()
	writer.Flush()
}

func GetSellingMonthly(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	query := params["id"]
	splits := strings.Split(query, "-")

	if len(splits) < 2 {
		http.Error(w, "Bad Request", 400)
		return
	}

	month, err := strconv.Atoi(splits[0])
	checkError(w, err)
	if err != nil {
		return
	}

	year, err := strconv.Atoi(splits[1])
	checkError(w, err)
	if err != nil {
		return
	}

	startTime := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)
	if month == 12 {
		month = 1
		year++
	} else {
		month++
	}

	endTime := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)

	endLabelTime, err := time.ParseDuration("-1ns")
	checkError(w, err)
	if err != nil {
		return
	}

	db, err := OpenDB()
	checkError(w, err)
	rows, err := db.Query("SELECT ID, DATE_UPDATED, SKU, DESCRIPTION, OUT, PRICE, TOTAL, AVERAGE, REVENUE FROM OUTGOING_REPORT_VIEW WHERE DATE_UPDATED >= $1 AND DATE_UPDATED < $2", startTime, endTime)
	checkError(w, err)

	var selling [][]string
	var id int
	var date time.Time
	var sku string
	var description string
	var out int
	var price int
	var total int
	var average int
	var revenue int

	idCount := 0
	revenueTotal := 0
	totalTotal := 0
	outTotal := 0

	for rows.Next() {
		err = rows.Scan(&id, &date, &sku, &description, &out, &price, &total, &average, &revenue)
		checkError(w, err)
		idCount++
		revenueTotal += revenue
		totalTotal += total
		outTotal += out

		selling = append(selling, []string{strconv.Itoa(id), date.String(), sku, description, strconv.Itoa(out), strconv.Itoa(price), strconv.Itoa(total), strconv.Itoa(average), strconv.Itoa(revenue)})
	}

	w.Header().Set("Content-Disposition", "attachment; filename=laporan penjualan.csv")
	w.Header().Set("Content-Type", "text/csv")
	writer := csv.NewWriter(w)
	writer.Write([]string{"LAPORAN PENJUALAN"})
	writer.Write([]string{""})
	writer.Write([]string{"Tanggal Cetak", time.Now().String()})
	writer.Write([]string{"Tanggal", strings.Join([]string{startTime.String(), "-", endTime.Add(endLabelTime).String()}, "")})
	writer.Write([]string{"Total Omzet", strconv.Itoa(totalTotal)})
	writer.Write([]string{"Total Laba Kotor", strconv.Itoa(revenueTotal)})
	writer.Write([]string{"Total Penjualan", strconv.Itoa(idCount)})
	writer.Write([]string{"Total Barang", strconv.Itoa(outTotal)})
	writer.Write([]string{""})
	writer.Write([]string{"ID Pesanan", "Waktu", "SKU", "Nama Barang", "Jumlah", "Harga Jual", "Total", "Harga Beli", "Laba"})
	writer.WriteAll(selling)

	rows.Close()
	db.Close()
	writer.Flush()
}

func Migration(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("migration.gtpl")

	t.Execute(w, nil)
}

func UploadStocks(w http.ResponseWriter, r *http.Request) {
	file, _, err := r.FormFile("uploadfile")
	checkError(w, err)
	if err != nil {
		return
	}
	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	checkError(w, err)
	if err != nil {
		return
	}

	db, err := OpenDB()
	checkError(w, err)

	_, err = db.Exec("DELETE FROM INCOMING")
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}
	_, err = db.Exec("DELETE FROM OUTGOING")
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}
	_, err = db.Exec("DELETE FROM STOCK")
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	stmt, err := db.Prepare("INSERT INTO STOCK(SKU, DESCRIPTION) VALUES ($1,$2)")
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	for _, v := range records {

		_, err = stmt.Exec(v[0], v[1])
	}

	db.Close()
	json.NewEncoder(w).Encode("Success")
}

func UploadIncoming(w http.ResponseWriter, r *http.Request) {
	file, _, err := r.FormFile("uploadfile")
	checkError(w, err)
	if err != nil {
		return
	}
	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	checkError(w, err)
	if err != nil {
		return
	}

	db, err := OpenDB()
	checkError(w, err)

	_, err = db.Exec("DELETE FROM INCOMING")
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	stmt, err := db.Prepare("INSERT INTO INCOMING(ID, SKU, ORDERED, RECEIVED, PRICE, RECEIPT, NOTE, DATE_UPDATED) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)")
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	for _, v := range records {
		_, err = stmt.Exec(v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7])
	}

	db.Close()
	json.NewEncoder(w).Encode("Success")
}

func UploadOutgoing(w http.ResponseWriter, r *http.Request) {
	file, _, err := r.FormFile("uploadfile")
	checkError(w, err)
	if err != nil {
		return
	}
	defer file.Close()

	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	checkError(w, err)
	if err != nil {
		return
	}

	db, err := OpenDB()
	checkError(w, err)

	_, err = db.Exec("DELETE FROM OUTGOING")
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	stmt, err := db.Prepare("INSERT INTO OUTGOING(ID, SKU, OUT, PRICE, NOTE, DATE_UPDATED) VALUES ($1,$2,$3,$4,$5,$6)")
	checkError(w, err)
	if err != nil {
		db.Close()
		return
	}

	for _, v := range records {
		_, err = stmt.Exec(v[0], v[1], v[2], v[3], v[4], v[5])
	}

	db.Close()
	json.NewEncoder(w).Encode("Success")
}
